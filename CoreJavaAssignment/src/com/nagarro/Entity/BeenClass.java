package com.nagarro.Entity;

public class BeenClass {
	private String car_model;
	private String car_type;
	private long car_cost_price;
	private long insurance_calculation;
	
	public void setCar_model(String car_model) {
		this.car_model = car_model;
	}
	public String getCar_type() {
		return car_type;
	}
	public String getCar_model() {
		return car_model;
	}
	public void setCar_type(String car_type) {
		this.car_type = car_type;
	}
	public long getCar_cost_price() {
		return car_cost_price;
	}
	public void setCar_cost_price(long car_cost_price) {
		this.car_cost_price = car_cost_price;
	}
	public long getInsurance_calculation() {
		return insurance_calculation;
	}
	public void setInsurance_calculation(long insurance_calculation) {
		this.insurance_calculation = insurance_calculation;
	}
	
	

}
