package com.nagarro.Select;
import java.util.ArrayList;
import java.util.Scanner;

import com.nagarro.Entity.BeenClass;
import com.nagarro.Implement.Car_Imp_Class;
public class Car_Insurance {
	public static void main(String args[])
	{
		
		Scanner sc=new Scanner(System.in);
		String Choice="y";
		ArrayList al = new ArrayList();
		Car_Imp_Class cl=new Car_Imp_Class();
		BeenClass bc=new BeenClass();
		
		while(Choice.equalsIgnoreCase("y"))
		{
	    System.out.println("Car Model");
		String car_model=cl.car_Model();
		bc.setCar_model(car_model);
	
		System.out.println("Car Type");
		String car_type = cl.car_Type();
		bc.setCar_type(car_type);
		
		System.out.println("Car cost price");
		long car_cost_price= cl.car_Cost_Price();
		bc.setCar_cost_price(car_cost_price);
		 
		System.out.println("Insurace type");
		String insurance_type = cl.car_Insurance_Type();
		
		long insurance_calculation=cl.car_Insurance_calculation(insurance_type,car_type,car_cost_price);
		bc.setInsurance_calculation(insurance_calculation);
		
		cl.display(bc);
		System.out.println("Do you want to enter details of any other car (y/n)"); 
		Choice=sc.nextLine();
		
	}
	
	

	}
}
