package com.nagarro.Implement;

import java.util.Scanner;

import com.nagarro.Entity.BeenClass;

public class Car_Imp_Class {
	Scanner sc=new Scanner(System.in);
	
	
	
//	method created for getting input car_model from user
	public String car_Model()
	{
		return sc.nextLine();
	}
	
	
	
	
//	method created for getting input car_type from user
	public String car_Type()
	{
		
		System.out.println("Car Type can have 3 possible values Hatchback, Sedan and SUV choose anyone");
	    String car_type = sc.nextLine();
	    if(car_type.equalsIgnoreCase("Hatchback") || car_type.equalsIgnoreCase("Sedan") || car_type.equalsIgnoreCase("SUV"))
	    {
	    	return car_type;
	    }
	    else
	    	car_Type();
	return car_type;
	}
	
	
	
	
//	method created for getting input car_cost_price from user
	public long car_Cost_Price()
	{
		
		long car_cost_price = sc.nextLong();
	    if(car_cost_price>100000)
	    {
	    	return car_cost_price;
	    }
	    else
	    	car_Cost_Price();
	return car_cost_price;
	}
	
	
	
	
//	method created for getting input car_cost_price from user
	public String car_Insurance_Type()
	{
		System.out.println("Insurance Type can have 2 possible values Basic and Premium choose anyone");
		sc.nextLine();
		String insurance_type = sc.nextLine();
		while(insurance_type.equalsIgnoreCase("Basic")||insurance_type.equalsIgnoreCase("Premium"))
		  		    return insurance_type;
		           return insurance_type;
		}
	
	
	
	
	
//	method created for getting calculation of insurance.
	public long car_Insurance_calculation(String insurance_type,String car_type,long car_cost_price)
	{
		long insurance_calculation=0;
		System.out.println(insurance_type);
		if(insurance_type.equalsIgnoreCase("Basic"))
		{
		if(car_type.equalsIgnoreCase("Hatchback"))
		{
			insurance_calculation = (car_cost_price*5)/100;
		}
		if(car_type.equalsIgnoreCase("Sedan"))
		{
			insurance_calculation = (car_cost_price*8)/100;
		}
		if( car_type.equalsIgnoreCase("SUV"))
		{
			insurance_calculation = (car_cost_price*10)/100;
		}
		}
		if(insurance_type.equalsIgnoreCase("Premium"))
		{
		if(car_type.equalsIgnoreCase("Hatchback")) 
		{
			insurance_calculation =(car_cost_price*5)/100;
			insurance_calculation = insurance_calculation+insurance_calculation*20/100;
		}
		if( car_type.equalsIgnoreCase("Sedan"))
		{
			insurance_calculation = (car_cost_price*8)/100;
			insurance_calculation = insurance_calculation+insurance_calculation*20/100;
		}
		if(car_type.equalsIgnoreCase("SUV"))
		{
			insurance_calculation = (car_cost_price*10)/100;
			insurance_calculation = insurance_calculation+insurance_calculation*20/100;
		}
		}
		return insurance_calculation;
	}
	
	
	
//	method created for display the output.
	public void display(BeenClass bc) 
	{
		System.out.println("output:");
	    System.out.println("Car Model :                   "+bc.getCar_model()+"");
	    System.out.println("price     :                   "+bc.getCar_cost_price()+"");
	    System.out.println("total insurance to be paid  : "+bc.getInsurance_calculation()+"");
			
	}
	
	
	}
	
